//Objects
// let obj={
//     Name:"Raghav",
//     CLass:"A2",
//     Roll_No:"16EIACS060",
//     City:"Faridabad",
//     Age:"22"
// }
// delete obj["Name"];
// // obj.Name="Raghav";
// obj ["Name"] = "Bob";
// obj.percent="75%";
// // console.log(obj);

// // console.log(`My age is `+` `+ obj.Age);
// console.log(`MY name is`+` `+ obj.Name);
// // console.log(`My percentage is `+` `+obj.percent);

// // console.log(typeof obj);

//Strings
// let firstName="Raghavesh";
// let lastName="Tripathi"
// let myInitials= firstName.charAt(0).concat(firstName.charAt(8));
// console.log(myInitials);

// let a = "My name is Raghav and i am 22 years old";
// // console.log(a.replace("Raghav","Bob"));
// // console .log(a.replace("Bob","Raghav"));
// console.log(a.split(" "));

//Spread Operator
// console.log(Math.max(4,5,34,2,4,23,2432,34,));

// var a=[4,5,34,2,4,23,2432,34];
// console.log(Math.max(...a));

// var a={
//     name:"Raghav",
//     age:22,
//     class:"A2",
//     rollno:60,
//     info:function(){
//     return `My name is ${a.name} and my age is ${this.age} along with my roll no ${this.rollno}`
// }
// }
// console.log(a.info());

// sum =0;
// function show(...x)
// {
// for (i of x)
// sum=sum+i
// return sum
// }
// console.log(show(5,4,3,2))

//program of if and nested if
// var a = 21;
// if (typeof a == "string") {
//   if ((a = 21)) {
//     console.log("the given number is absolutely true");
//   }
// }

//program of if and else if

// var a=(21);
// if(a==22){
//     console.log("The program is running smoothly");

// }
// else{
//     console.log("there is an error");

// }

//Making the same program with ternary operator

// var a =(21);
// (a==21)? console.log("The program is running smoothly"): console.log("There is an error");

//program using switch statement

// var a = 21;
// switch (a) {
//   case 1:
//     console.log("The value is absoluetely wrong");
//     break;
//   case 2:
//     console.log("The value is absoluetely wrong");
//     break;
//   case 213:
//     console.log("The value is absoluetly right");
//     break;
//   default:
//     console.log("values which you are typing are not present");
// }

//Switch stament program number 2

// var raghav = 23;
// switch (raghav) {
//   case 1:
//     console.log("Value is wrong");
//     break;
//   case 2:
//     console.log("Value is wrong");
//     break;
//   case 23:
//     console.log("value is right");
//     break;
//   default:
//     console.log("We are done woth the code");
// }

// var raghav = 21;
// switch (raghav) {
//   case 1:
//     console.log("the value is wrong");
//     break;
//   case 2:
//     console.log("the value is wrong");
//     break;
//   case 23:
//     console.log("the value is correct");
//     break;
//   default:
//     console.log("the value is just as usual");
// }




